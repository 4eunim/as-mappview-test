(*********************************************************************************
 * Copyright: B&R Industrial Automation GmbH 
 * Author:    ime 
 * Created:   July 14, 2022/5:52 PM 
 *********************************************************************************)

PROGRAM _INIT
	
END_PROGRAM

PROGRAM _CYCLIC
	

	//Muti Text
	strTextExchange;
	
	IF bChangeText THEN

		brsstrcpy(ADR(strTotal),ADR(strPrefix));
		brsstrcat(ADR(strTotal),ADR(strNexID)); //hello
		
		brsstrcpy(ADR(strTextExchange),ADR(strTotal));
		
	ELSE
		brsstrcpy(ADR(strTotal),ADR(strPrefix));
		brsstrcat(ADR(strTotal),ADR('hi'));
		
		brsstrcpy(ADR(strTextExchange),ADR(strTotal));
	END_IF;
	
	
	//Gestures, 69ea1881-f7c9-4fd5-842f-cdefd49964d9
	
	
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

