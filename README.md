# Sample Project for each mapp View widgets

첫 작성일   : 2022/9/8
마지막 수정일: 2022/9/8

B&R PLC에서 작동하는 mappView의 위젯 각각의 기능을 확인하기 위한 테스트용 샘플 프로젝트입니다.

Tool 
- B&R Automation Studio 4.11 이상

mappView 연결정보
- 웹브라우저에서 입력 주소:  127.0.0.1:81

프로젝트 명명 규칙
- mappViewSupport_<위치이름>_<옵션: 버전이름>

프로젝트 저장 위치
- Project\

프로젝트별 설명
- mappViewSupport_motionPad_v1: motion pad 테스트용
- mappViewSupport_motionPad_v2: motion pad의 event binding에서 연결될  static file path to dynamic filepath using string하는 방법

 
 자세한 내용은 추후 업데이트 예정...

